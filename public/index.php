<?php

use Envms\FluentPDO\Query;
use Oop\Notes\Helpers\Request;

require __DIR__ .'/../vendor/autoload.php';
require_once __DIR__ . '/../routes/web.php';

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);

$uri = explode('?', $_SERVER['REQUEST_URI'], 2)[0];

$class = $routes[$uri]['controller'];
$method = $routes[$uri]['method'];
$protected = isset($routes[$uri]['protected']) ? $routes[$uri]['protected'] : true;

$pdo = new PDO('mysql:dbname=notes;host=localhost', 'root', 'password');

$request = new Request();
$controller = new $class($request, new Query($pdo));

if ($protected && !$request->getSession()->has('user')) {
    header('Location: /login');
}

$controller->$method();


