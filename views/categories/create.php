<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Notes</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/water.css@2/out/dark.css">
</head>
<body>
    <h1>Categories</h1>
    <a href="/">Home</a>|
    <a href="/logout">Logout</a> 
    <hr>
    <form action="/categories/store" method="post">
        <label for="name">Name</label>
        <input type="text" name="name" id="name">

        <label for="position">Position</label>
        <input type="number" name="position" id="position">

        <input type="reset" value="Rest" style="float: left;">
        <input type="submit" value="Create">
    </form>
</body>
</html>
