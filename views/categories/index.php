<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Notes</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/water.css@2/out/dark.css">
</head>
<body>
    <h1>Categories</h1>
    <a href="/">Home</a> |
    <a href="/logout">Logout</a> |
    <a href="/categories/create">New Category</a>
    <hr>
    <table>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Position</th>
            <th>Actions</th>
        </tr>
        <?php foreach ($categories as $category) { ?>
        <tr>
            <td><?= $category['id'] ?></td>
            <td><?= $category['name'] ?></td>
            <td><?= $category['position'] ?></td>
            <td>
                <a href="/categories/edit?id=<?= $category['id'] ?>">Edit</a> |
                <a href="/categories/show?id=<?= $category['id'] ?>">View</a> |
                <a href="/categories/delete?id=<?= $category['id'] ?>">Delete</a>
            </td>
        </tr>
        <?php } ?>
    </table>
</body>
</html>
