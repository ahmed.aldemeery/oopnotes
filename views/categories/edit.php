<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Notes</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/water.css@2/out/dark.css">
</head>
<body>
    <h1>Categories</h1>
    <a href="/">Home</a> |
    <a href="/logout">Logout</a> 
    <hr>
    <form action="/categories/update?id=<?= $category['id'] ?>" method="post">
        <label for="name">Name</label>
        <input type="text" name="name" id="name" value="<?= $category['name'] ?>">

        <label for="position">Position</label>
        <input type="number" name="position" id="position" value="<?= $category['position'] ?>">

        <input type="reset" value="Rest" style="float: left;">
        <input type="submit" value="Update">
    </form>
</body>
</html>
