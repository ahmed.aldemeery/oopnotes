<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Notes</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/water.css@2/out/dark.css">
</head>
<body>
    <h1>Notes</h1>
    <a href="/">Home</a> |
    <a href="/logout">Logout</a> |
    <a href="/notes/create">New Note</a>
    <hr>
    <table>
        <tr>
            <th>ID</th>
            <th>Title</th>
            <th>Content</th>
            <th>Position</th>
            <th>Actions</th>
        </tr>
        <?php foreach ($notes as $note) { ?>
        <tr>
            <td><?= $note['id'] ?></td>
            <td><?= $note['title'] ?></td>
            <td><?= $note['content'] ?></td>
            <td><?= $note['position'] ?></td>
            <td>
                <a href="/notes/edit?id=<?= $note['id'] ?>">Edit</a> |
                <a href="/notes/show?id=<?= $note['id'] ?>">View</a> |
                <a href="/notes/delete?id=<?= $note['id'] ?>">Delete</a>
            </td>
        </tr>
        <?php } ?>
    </table>
</body>
</html>
