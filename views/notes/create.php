<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Notes</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/water.css@2/out/dark.css">
</head>
<body>
    <h1>Notes</h1>
    <a href="/">Home</a> |
    <a href="/logout">Logout</a> 
    <hr>
    <form action="/notes/store" method="post">
        <label for="Title">Title</label>
        <input type="text" name="title" id="title">

        <select name="categories[]" id="categories" multiple>
        <?php foreach ($categories as $category) {?> 
            <option  value="<?php echo $category['id'];?>" ><?php echo $category['name'];?></option>
        <?php }?>
        </select>
        
        <label for="content">Content</label>
        <input type="text" name="content" id="content" width="80" height="100" >  
        
        <label for="position">Position</label>
        <input type="number" name="position" id="position">

        <input type="reset" value="Rest" style="float: left;">
        <input type="submit" value="Create">
    </form>
</body>
</html>
