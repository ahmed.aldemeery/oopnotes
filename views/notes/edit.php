<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Notes</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/water.css@2/out/dark.css">
</head>
<body>
    <h1>Notes</h1>
    <a href="/">Home</a> |
    <a href="/logout">Logout</a> 
    <hr>
    <form action="/notes/update?id=<?= $note['id'] ?>" method="post">

        <label for="title">Title</label>
        <input type="text" name="title" id="title" value="<?= $note['title'] ?>">

        <select name="categories[]" id="categories" multiple>
        <?php foreach ($categories as $category) {?> 
            <?php if (in_array($category['id'], $selected)) { ?>
                <option  value="<?php echo $category['id'];?>" <?= 'selected' ?>><?php echo $category['name'];?></option>
            <?php } else { ?>
                <option  value="<?php echo $category['id'];?>"><?php echo $category['name'];?></option>
            <?php } ?>
        <?php }?>
        </select>

        <label for="title">Content</label>
        <input type="text" name="content" id="content" value="<?= $note['content'] ?>">
          
        <label for="position">Position</label>
        <input type="number" name="position" id="position" value="<?= $note['position'] ?>">

        <input type="reset" value="Rest" style="float: left;">
        <input type="submit" value="Update">
    </form>
</body>
</html>
