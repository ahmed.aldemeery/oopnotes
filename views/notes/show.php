<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Notes</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/water.css@2/out/dark.css">
</head>
<body>
    <h1>Notes</h1>
    <a href="/">Home</a> |
    <a href="/logout">Logout</a> 
    <hr>
    <div>
        <h3>Title</h3>
        <?= $note['title'] ?>

        <h3>Content</h3>
        <?= $note['content'] ?>

        <h3>Position</h3>
        <?= $note['position'] ?>
    </div>
</body>
</html>
