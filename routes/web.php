<?php

$routes = [
    '/' => [
        'controller' => 'Oop\Notes\Controllers\HomeController',
        'method' => 'home',
    ],
    '/login' => [
        'controller' => 'Oop\Notes\Controllers\AuthController',
        'method' => 'login',
        'protected' => false,
    ],
    '/register' => [
        'controller' => 'Oop\Notes\Controllers\AuthController',
        'method' => 'register',
        'protected' => false,
    ],
    '/signin' => [
        'controller' => 'Oop\Notes\Controllers\AuthController',
        'method' => 'signin',
        'protected' => false,
    ],
    '/logout' => [
        'controller' => 'Oop\Notes\Controllers\AuthController',
        'method' => 'logout',
    ],
    '/signup' => [
        'controller' => 'Oop\Notes\Controllers\AuthController',
        'method' => 'signup',
        'protected' => false,
    ],

    // Categories.......
    '/categories/all' => [
        'controller' => 'Oop\Notes\Controllers\CategoryController',
        'method' => 'index',
    ],
    '/categories/show' => [
        'controller' => 'Oop\Notes\Controllers\CategoryController',
        'method' => 'show',
    ],
    '/categories/create' => [
        'controller' => 'Oop\Notes\Controllers\CategoryController',
        'method' => 'create',
    ],
    '/categories/store' => [
        'controller' => 'Oop\Notes\Controllers\CategoryController',
        'method' => 'store',
    ],
    '/categories/edit' => [
        'controller' => 'Oop\Notes\Controllers\CategoryController',
        'method' => 'edit',
    ],
    '/categories/update' => [
        'controller' => 'Oop\Notes\Controllers\CategoryController',
        'method' => 'update',
    ],
    '/categories/delete' => [
        'controller' => 'Oop\Notes\Controllers\CategoryController',
        'method' => 'delete',
    ],

    // notes......

    '/notes/all' => [
        'controller' => 'Oop\Notes\Controllers\NoteController',
        'method' => 'index',
    ],

    '/notes/show' => [
        'controller' => 'Oop\Notes\Controllers\NoteController',
        'method' => 'show',
    ],

    '/notes/create' => [
        'controller' => 'Oop\Notes\Controllers\NoteController',
        'method' => 'create',
    ],

    '/notes/store' => [
        'controller' => 'Oop\Notes\Controllers\NoteController',
        'method' => 'store',
    ],

    '/notes/edit' => [
        'controller' => 'Oop\Notes\Controllers\NoteController',
        'method' => 'edit',
    ],

    '/notes/update' => [
        'controller' => 'Oop\Notes\Controllers\NoteController',
        'method' => 'update',
    ],

    '/notes/delete' => [
        'controller' => 'Oop\Notes\Controllers\NoteController',
        'method' => 'delete',
    ],

    // users.......



];
