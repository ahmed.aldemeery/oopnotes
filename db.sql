CREATE DATABASE IF NOT EXISTS `notes`;

USE `notes`;

CREATE TABLE IF NOT EXISTS `users` (
    `id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `name` VARCHAR(255) NOT NULL,
    `email` VARCHAR(255) NOT NULL UNIQUE,
    `password` VARCHAR(255) NOT NULL,
    `created_at` TIMESTAMP NOT NULL DEFAULT NOW()
);

CREATE TABLE IF NOT EXISTS `categories` (
    `id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `user_id` INT(11) NOT NULL,
    `name` VARCHAR(255) NOT NULL,
    `position` INT(11),
    `created_at` TIMESTAMP NOT NULL DEFAULT NOW(),
    UNIQUE (`name`, `user_id`),
    FOREIGN KEY (`user_id`) REFERENCES `users`(`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
);
