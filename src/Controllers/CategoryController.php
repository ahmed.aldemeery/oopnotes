<?php

namespace Oop\Notes\Controllers;

use Envms\FluentPDO\Query;
use Oop\Notes\Helpers\Request;

class CategoryController
{
    private Request $request;

    private Query $db;

    public function __construct(Request $request, Query $db)
    {
        $this->request = $request;
        $this->db = $db;
    }

    public function index()
    {
        $user = $this->request->getSession()->get('user');
        $categories = $this->db->from('categories')->where('user_id', $user['id'])->fetchAll();

        include __DIR__ . "/../../views/categories/index.php";
    }

    public function show()
    {
        $id = $this->request->get('id');
        $category = $this->db->from('categories')->where('id', $id)->fetch();

        include __DIR__ . '/../../views/categories/show.php';
    }

    public function create()
    {
        include __DIR__ . "/../../views/categories/create.php";
    }

    public function store()
    {
        $user = $this->request->getSession()->get('user');
        $attributes = $this->request->post();
        $attributes['user_id'] = $user['id'];

        $this->db->insertInto('categories')->values($attributes)->execute();

        header('Location: /categories/all');
    }

    public function edit()
    {
        $id = $this->request->get('id');
        $category = $this->db->from('categories')->where('id', $id)->fetch();

        include __DIR__ . '/../../views/categories/edit.php';
    }

    public function update()
    {
        $id = $this->request->get('id');
        $attributes = $this->request->post();

        $this->db->update('categories')->set($attributes)->where('id', $id)->execute();

        header('Location: /categories/all');
    }

    public function delete()
    {
        $id = $this->request->get('id');

        $this->db->deleteFrom('categories')->where('id', $id)->execute();

        header("Location: /categories/all");
    }
}
