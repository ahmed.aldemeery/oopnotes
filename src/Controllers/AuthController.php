<?php

namespace Oop\Notes\Controllers;

use Envms\FluentPDO\Query;
use Oop\Notes\Helpers\Request;

class AuthController
{
    private Request $request;

    private Query $db;

    public function __construct(Request $request, Query $db)
    {
        $this->request = $request;
        $this->db = $db;
    }

    public function login()
    {
        if ($this->request->getSession()->has('user')) {
            header('Location: /');
            exit();
        }

        include __DIR__ . "/../../views/login.php";
    }

    public function logout()
    {
        $this->request->getSession()->destroy();
        header('Location: /login');
        exit();
    }

    public function register()
    {
        if ($this->request->getSession()->has('user')) {
            header('Location: /');
            exit();
        }

        include __DIR__ . "/../../views/register.php";
    }

    public function signup()
    {
        $data = $this->request->post();
        
        if ($data['password'] !== $data['password_confirmation']) {
            header('Location: /register');
            exit();
        }

        $user = $this->db->from('users')->where('email', $data['email'])->fetch();

        if ($user !== false) {
            header('Location: /register');
            exit();
        }

        unset($data['password_confirmation']);

        $this->db->insertInto('users')->values($data)->execute();

        header('Location: /');
        exit();
    }

    public function signin()
    {
        $email = $this->request->post('email');
        $password = $this->request->post('password');


        $user = $this->db
            ->from('users')
            ->where('email', $email)
            ->where('password', $password)
            ->fetch();


        if ($user == false){
            header('Location: /login');
            exit();
        } 
        
        $this->request->getSession()->put('user', $user);
        header('Location: /');
    }
}
