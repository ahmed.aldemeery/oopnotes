<?php

namespace Oop\Notes\Controllers;

use Oop\Notes\Helpers\Request;

class HomeController
{
    private Request $request;
    
    public function home()
    {
        include __DIR__ . "/../../views/home.php";
    }
}
