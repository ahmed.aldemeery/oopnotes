<?php

namespace Oop\Notes\Controllers;

use Envms\FluentPDO\Query;
use Oop\Notes\Helpers\Request;

class NoteController
{

    private Request $request;

    private Query $db;

    public function __construct(Request $request, Query $db)
    {
        $this->request = $request;
        $this->db = $db;
    }

    public function index()
    {
        $user = $this->request->getSession()->get('user');
        $notes = $this->db->from('notes')->where('user_id', $user['id'])->fetchAll();

        include __DIR__ . "/../../views/notes/index.php";
    }

    public function show()
    {
        $id = $this->request->get('id');
        $note = $this->db->from('notes')->where('id', $id)->fetch();

        include __DIR__ . '/../../views/notes/show.php';
    }


    public function create()
    {
        $user = $this->request->getSession()->get('user');
        $categories = $this->db->from('categories')->where('user_id', $user['id'])->fetchAll();
        
        include __DIR__ . "/../../views/notes/create.php";
    }

    public function store()
    {
        $user = $this->request->getSession()->get('user');
        $attributes = [
            'title' => $this->request->post('title'),
            'content' => $this->request->post('content'),
            'position' => $this->request->post('position'),
            'user_id' => $user['id'],
        ];

        $categoriesIds = $this->request->post('categories');

        $noteId = $this->db->insertInto('notes')->values($attributes)->execute();

        foreach ($categoriesIds as $categoryId) {

            $values = [
                'category_id' => $categoryId,
                'note_id' => $noteId,
            ];

            $this->db->insertInto('category_note')->values($values)->execute();
        }
    
        header('Location: /notes/all');
    }

    public function edit()
    {
        $id = $this->request->get('id');
        $note = $this->db->from('notes')->where('id', $this->request->get('id'))->fetch();

        $categories = $this->db->from('categories')->where('user_id', 1)->fetchAll();
        $cn = $this->db->from('category_note')->where('note_id', $id)->fetchAll();
        $selected = [];

        foreach ($cn as $row) {
            $selected[] = $row['category_id'];
        }
        
        include __DIR__ . '/../../views/notes/edit.php';
    }

    public function update()
    {
        $id = $this->request->get('id');
        $attributes = [
            'title' => $this->request->post('title'),
            'content' => $this->request->post('content'),
            'position' => $this->request->post('position'),
        ];

        $categoriesIds = $this->request->post('categories');
        
        $this->db->update('notes')->set($attributes)->where('id', $id)->execute();

        $this->db->deleteFrom('category_note')->where('note_id', $id)->execute();

        foreach ($categoriesIds as $categoryId) {

            $values = [
                'category_id' => $categoryId,
                'note_id' => $id,
            ];

            $this->db->insertInto('category_note')->values($values)->execute();
        }
        
        header('Location: /notes/all');
    }


    public function delete()
    {
        $id = $this->request->get('id');

        $this->db->deleteFrom('notes')->where('id', $id)->execute();

        header("Location: /notes/all");
    }

}
