<?php

namespace Oop\Notes\Helpers;

class Session
{
    public function __construct()
    {
        $this->start();
    }

    public function start()
    {
        session_start();
    }

    public function destroy()
    {
        session_unset();
        session_destroy();
    }

    public function get(string $key)
    {
        return isset($_SESSION[$key]) ? $_SESSION[$key] : null;
    }

    public function put(string $key, $value)
    {
        $_SESSION[$key] = $value;
    }

    public function has(string $key)
    {
        return !is_null($this->get($key));
    }
}