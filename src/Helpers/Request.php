<?php

namespace Oop\Notes\Helpers;

use Oop\Notes\Helpers\Session;

class Request
{
    private Session $session;
    
    private array $get = [];

    private array $post = [];

    public function __construct()
    {
        $this->session = new Session();
        $this->get = $_GET;
        $this->post = $_POST;
    }
    
    public function getSession()
    {
        return $this->session;
    }
    
    public function get(string $key = '')
    {
        if (empty($key)) {
            return $this->get;
        }

        if (isset($this->get[$key])) {
            return $this->get[$key];
        }

        return null;
    }

    public function post(string $key = '')
    {
        if (empty($key)) {
            return $this->post;
        }

        if (isset($this->post[$key])) {
            return $this->post[$key];
        }

        return null;
    }
}
